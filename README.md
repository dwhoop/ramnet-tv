If you use this or a modified version of it, please give us (ramnet e.V) some credit :)

If you would like to continue developing this app (maybe integrate an epg?) please contact us at support@ram.rwth-aachen.de

# ramnet-tv

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1.

## Build & development

Run `grunt build` for building and `grunt serve` for preview.
Development happens in app/ and the build files will be stored in dist/

## Testing

Running `grunt test` will run the unit tests with karma.
