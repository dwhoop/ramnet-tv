'use strict';

/**
 * @ngdoc function
 * @name ramnetTvApp.controller:WatchCtrl
 * @description
 * # WatchCtrl
 * Controller of the ramnetTvApp
 */
angular.module('ramnetTvApp')
  .controller('WatchCtrl', function ($scope, $routeParams, $http, $log, $sce) {

    $scope.channelID = $routeParams.key;
    $scope.channelName = $routeParams.val;

    $log.info($routeParams);

    $scope.videoAPI = null;

    $scope.onPlayerReady = function(API) {
      $scope.videoAPI = API;
    };

    $scope.videoConfig = {
      preload: "none",
      sources: [
        {src: $sce.trustAsResourceUrl('http://tv.ram.rwth-aachen.de:9981/stream/channel/' + $scope.channelID + '?profile=webapp'), type: 'video/mp4'}
      ],
      theme: "/styles/videogular.css",
      plugins: {
        poster: 'https://www.ram.rwth-aachen.de/templates/design_control/images/s5_logo.png',
        controls: {
            autoHide: true,
            autoHideTime: 2000
        }
      }
    };


  }
);
