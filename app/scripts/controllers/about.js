'use strict';

/**
 * @ngdoc function
 * @name ramnetTvApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ramnetTvApp
 */
angular.module('ramnetTvApp')
  .controller('AboutCtrl', function ($scope) {

    $scope.angularVersion = angular.version.full + ' ' + angular.version.codeName;
    $scope.appVersion = '0.0.1';
  });
