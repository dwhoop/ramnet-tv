'use strict';

/**
 * @ngdoc overview
 * @name ramnetTvApp
 * @description
 * # ramnetTvApp
 *
 * Main module of the application.
 */
angular
  .module('ramnetTvApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',

    'com.2fdevs.videogular',
    'com.2fdevs.videogular.plugins.controls',
    'com.2fdevs.videogular.plugins.overlayplay',
    'com.2fdevs.videogular.plugins.poster'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        redirectTo: '/channels'
        // templateUrl: 'views/main.html',
        // controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/channels', {
        templateUrl: 'views/channels.html',
        controller: 'ChannelsCtrl'
      })
      .when('/watch/:key/:val', {
        templateUrl: 'views/watch.html',
        controller: 'WatchCtrl'
      })
      .otherwise({
        //redirectTo: '/'
        templateUrl: '404.html',
        controller: 'MainCtrl'
      });

  });
