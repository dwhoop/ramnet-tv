'use strict';

/**
 * @ngdoc service
 * @name ramnetTvApp.channelService
 * @description
 * # channelService
 * Service in the ramnetTvApp.
 */
angular.module('ramnetTvApp')
  .service('channelService', function ($log, $http) {

    this.logPrefix = '[channelService] ';

    //this.channelApiUrl = 'http://tv.ram.rwth-aachen.de:9981/api/channel/list';
    this.channelApiUrl = '/channels.json';

    this.getChannelList = function( $scope ) {
      $http.get(this.channelApiUrl)
          .success(function(data) {
              $scope.channels = data.entries;
              //$log.info(data.entries);
          })
          .error(function(status, res) {
              $scope.channels = [];
              $scope.message = 'Failed to get the channel list - status:' + status;
          });
    };


  });
